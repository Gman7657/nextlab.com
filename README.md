# next.gitlab.com

This repository contains the static content for next.gitlab.com which allows users to
toggle whether they will be directed to the
[canary infrastructure](https://gitlab.com/gitlab-org/release/docs/blob/master/general/deploy/canary.md).


## Development

Assets are located in the `public/` directory, to test locally run:

```
make serve
```

point your browser to http://localhost:8000
